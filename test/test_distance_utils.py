"""
Test Module for Coordinate and DistanceUtil classes.
"""
from unittest import TestCase
from intercom_party.distance_utils import Coordinate, DistanceUtils


class TestCoordinate(TestCase):
    def test_to_radians(self):
        """
        Test degrees to radians
        """
        # Coordinate table, that we are going to test.
        coordinates_to_test = [
            Coordinate(latitude="53.339428", longitude="-6.257664"),
            Coordinate(latitude="0", longitude="0"),
            Coordinate(latitude="52.833502", longitude="-8.522366"),
        ]
        expected_results = [
            (0.9309486397304539, -0.10921684028351844),
            (0.0, 0.0),
            (0.9221185652590091, -0.14874334676001907),
        ]
        for index, coordinate in enumerate(coordinates_to_test):
            returned_lat_rad, returned_long_rad = coordinate.to_radians()
            self.assertEqual(expected_results[index][0], returned_lat_rad)
            self.assertEqual(expected_results[index][1], returned_long_rad)


class TestDistanceUtils(TestCase):
    def test_distance_between_two_points_on_a_sphere(self):
        """
        Test distances on a sphere as per Great-Circle distance method.
        """
        expected_distance = 126.63795221077943
        first_coordinate = Coordinate(latitude="53.339428", longitude="-6.257664")
        second_coordinate = Coordinate(latitude="52.833502", longitude="-8.522366")
        distance_result = DistanceUtils().distance_between_two_points_on_a_sphere(
            first_coordinate=first_coordinate, second_coordinate=second_coordinate, sphere_radius=5000
        )
        self.assertEqual(expected_distance, distance_result)

        # If you don't pass the sphere radius, earth radius is considered.
        expected_distance_on_earth = 161.36230665528913
        distance_result_on_earth = DistanceUtils().distance_between_two_points_on_a_sphere(
            first_coordinate=first_coordinate, second_coordinate=second_coordinate)
        self.assertEqual(expected_distance_on_earth, distance_result_on_earth)

