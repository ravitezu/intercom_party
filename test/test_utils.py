"""
Module to help us with the Test Utils
"""
from pathlib import Path
from tempfile import NamedTemporaryFile
from intercom_party.distance_utils import Coordinate

# Constants:
TEST_CUSTOMERS_FILE_PATH = "fixtures/test_customers.txt"


class TestUtils:
    """
    TestUtils class containing the utils for tests.
    """

    @staticmethod
    def get_test_customers_file_path():
        """
        Returns: test customers file path.
        """
        curr_file_path = Path(__file__)
        return str(Path.joinpath(curr_file_path.parent, Path(TEST_CUSTOMERS_FILE_PATH)))

    @staticmethod
    def get_empty_file(suffix=".txt"):
        """
        Creates an empty file and returns its name.
        Returns: Empty file path.
        """
        temp_file = NamedTemporaryFile(suffix=suffix)
        return temp_file.name

    @staticmethod
    def get_test_coordinate():
        """
        Returns: A test coordinate
        """
        return Coordinate(latitude="53.339428", longitude="-6.257664")

