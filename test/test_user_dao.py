"""
Module to test UserDAO implementation classes.
"""
from unittest import TestCase
import responses
from test.test_utils import TestUtils
from intercom_party.dao.user_dao import LocalFileReader, RemoteFileReader


class TestUserDAO(TestCase):
    def test_get_all_users_with_in_range(self):
        reader = LocalFileReader(TestUtils().get_test_customers_file_path())
        from_coordinate = TestUtils().get_test_coordinate()
        expected_users_within_range = 1
        expected_user_user_id = 12
        returned_users = reader.get_all_user_with_in_range(from_coordinate=from_coordinate, search_radius=100.0)
        self.assertEqual(len(returned_users), expected_users_within_range)
        self.assertEqual(returned_users[0][0], expected_user_user_id)


class TestLocalFileReader(TestCase):
    def test_get_all_users_with_test_data_file(self):
        lfr = LocalFileReader(TestUtils().get_test_customers_file_path())
        lfr_users = []
        for user in lfr.get_all_users():
            lfr_users.append(user)
        self.assertEqual(len(lfr_users), 6)

    def test_get_all_users_with_no_file(self):
        lfr = LocalFileReader(TestUtils().get_empty_file())
        with self.assertRaises(FileNotFoundError):
            lfr_users = []
            for user in lfr.get_all_users():
                lfr_users.append(user)


class TestRemoteFileReader(TestCase):
    @responses.activate
    def test_get_all_users_with_test_data_file(self):
        test_url = "https://somedummy/url/customers.txt"
        with open(TestUtils.get_test_customers_file_path(), "rb") as user_file:
            responses.add(
                responses.GET,
                test_url,
                body=user_file.read(),
                status=200,
                stream=True,
            )
        rfr = RemoteFileReader(test_url)
        rfr_users = []
        for user in rfr.get_all_users():
            rfr_users.append(user)
        self.assertEqual(len(rfr_users), 6)
