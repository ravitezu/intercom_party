"""
Module to test the User class
"""
from unittest import TestCase
from intercom_party.user import User


class TestUser(TestCase):
    def test_get_user_coordinates(self):
        """
        Test user coordinates
        """
        expected_lat = "52.966"
        expected_long = "-6.463"
        user = User(1, "ravi", expected_lat, expected_long)
        res_coordinates = user.get_user_coordinates()
        self.assertEqual(expected_lat, res_coordinates.latitude)
        self.assertEqual(expected_long, res_coordinates.longitude)

