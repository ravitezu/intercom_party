# Intercom-Party

The solution is implemented in Python(3.8.6). Please see the [Usage](https://gitlab.com/ravitezu/intercom_party#usage) section on how to run the code.
Note: Please make sure to use python3/pip3 in the following commands if Python2.x is default on your machine.

## Problem Statement:
We have some customer records in a text file (customers.txt) -- one customer per line, JSON
lines formatted. We want to invite any customer within 100km of our Dublin office for some food
and drinks on us. Write a program that will read the full list of customers and output the names
and user ids of matching customers (within 100km), sorted by User ID (ascending).
- You must use the first formula from this Wikipedia article(https://en.wikipedia.org/wiki/Earth_radius) to calculate
distance. Don't forget, you'll need to convert degrees to radians.
- The GPS coordinates for our Dublin office are 53.339428, -6.257664.
- You can find the Customer list here(https://s3.amazonaws.com/intercom-take-home-test/customers.txt).

## Installation steps: 
1. If you don't have Python installed on your machine, please visit [www.python.org](https://www.python.org/downloads/) to download and install it.  
2. Clone this repo using git: `git clone git@gitlab.com:ravitezu/intercom_party.git`
3. Go to the cloned directory: `cd intercom_party`
4. You can install the project dependencies using `pip install -r requirements.txt`
5. (Optional) If you want to run the unit tests, you need to install the test dependencies: `pip install -r requirements-test.txt`

## Usage:
1. Go to the parent directory of the `intercom_party`(repository root).
2. Run `python intercom_party --help` to see the help: 
```
usage: intercom_party [-h] [--range RANGE] [--url URL | --file FILE]

Find users with in a search radius from our Dublin office

optional arguments:
  -h, --help     show this help message and exit
  --range RANGE  Search radius from Dublin office (default: 100.0)
  --url URL      URL to read the users data (default: None)
  --file FILE    File path to read the users data (default: None)
```
3. As you can see in the usage, you can pass a file path or a URL in the command. We can include as many data sources as we want by implementing the abstract base class `UserDAO`. 
4. Test `customers.txt` has been added to this repository here: intercom_party/data/customers.txt
5. So you can pass the file to the executable as: `python intercom_party --file intercom_party/data/customers.txt`
6. Or you can give it a URL: `python intercom_party --url https://s3.amazonaws.com/intercom-take-home-test/customers.txt`
7. You can also pass the range(default: 100.0 KM) to search for the users: `python intercom_party --range 50 --file intercom_party/data/customers.txt`
8. The executable will read the users data and calculate the radius from our Dublin office to each user. If the user is within the passed range(default: 100.0 KM) then, it will print that user details(ID, Name).

## Runs Output:
Runs output is stored in the [output.txt](https://gitlab.com/ravitezu/intercom_party/-/blob/master/output.txt) file.  

## Running tests:
1. Tests are stored in the [test](https://gitlab.com/ravitezu/intercom_party/-/tree/master/test) folder in the repository root.
2. Go to the repository root folder: `cd intercom_party` and run `python -m unittest discover -vvv` to run all tests.
```
python -m unittest discover -vvv
test_to_radians (test.test_distance_utils.TestCoordinate)
Test degrees to radians ... ok
test_distance_between_two_points_on_a_sphere (test.test_distance_utils.TestDistanceUtils)
Test distances on a sphere as per Great-Circle distance method. ... ok
test_get_user_coordinates (test.test_user.TestUser)
Test user coordinates ... ok
test_get_all_users_with_no_file (test.test_user_dao.TestLocalFileReader) ... ok
test_get_all_users_with_test_data_file (test.test_user_dao.TestLocalFileReader) ... ok
test_get_all_users_with_test_data_file (test.test_user_dao.TestRemoteFileReader) ... ok
test_get_all_users_with_in_range (test.test_user_dao.TestUserDAO) ... ok

----------------------------------------------------------------------
Ran 7 tests in 0.013s

OK
```