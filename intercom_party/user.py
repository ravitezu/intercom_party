"""
User Module which consists of a User Class and related methods.
"""
from dataclasses import dataclass
from intercom_party.distance_utils import Coordinate


@dataclass
class User:
    """
    User class
    """
    user_id: int
    """ User ID """
    name: str
    """ User Name """
    latitude: str
    """ User Latitude """
    longitude: str
    """ User Longitude """

    def get_user_coordinates(self) -> Coordinate:
        """
        Method to return the user coordinates as Coordinate object
        Returns: Returns the coordinate object.
        """
        return Coordinate(latitude=self.latitude, longitude=self.longitude)
