"""
This module contains Coordinate and DistanceUtil classes and methods required to calculate distances.
"""

from typing import Tuple
from dataclasses import dataclass
from typing import Optional
from math import sin, cos, acos, radians

# Constants:
# Earth radius in 6371.009 KM approximately.
EARTH_RADIUS = 6371.009


@dataclass
class Coordinate:
    """
    Class to represent a coordinate (with Latitude and Longitude)
    """
    latitude: str
    """ Latitude """
    longitude: str
    """ Longitude """

    def to_radians(self) -> Tuple[float, float]:
        """
        Method to convert coordinate in degrees to radians.
        Returns:
            Returns the tuple of coordinate latitude and longitude in radians.
        """
        lat_rad, long_rad = map(radians, [float(self.latitude), float(self.longitude)])
        return lat_rad, long_rad


class DistanceUtils:
    """
    Utils class for calculating distances between coordinates.
    """

    @staticmethod
    def distance_between_two_points_on_a_sphere(
            first_coordinate: "Coordinate", second_coordinate: "Coordinate", sphere_radius: Optional[float] = None
    ):
        """
        This method implements the Great-circle distance method for calculating the distance between the 2 coordinates.
        See: https://en.wikipedia.org/wiki/Great-circle_distance for more details.
        Args:
            first_coordinate: First coordinate object to calculate the distance to second_coordinate.
            second_coordinate: Second coordinate object to calculate the distance to first_coordinate.
            sphere_radius: Radius of the sphere on which the points are located.
            If not provided, it assumed that the points are located on the Earth and distance is calculated accordingly.
        Returns:
            Returns the distance between the coordinates in KM(float rounder over 2 decimals)
        """
        # If sphere_radius is not provided, we take Earth radius.
        if sphere_radius is None:
            sphere_radius = EARTH_RADIUS

        curr_lat_in_rad, curr_long_in_rad = first_coordinate.to_radians()
        other_lat_in_rad, other_long_in_rad = second_coordinate.to_radians()

        # Calculate and return the Great-circle distance between two coordinates.
        return sphere_radius * (
            acos(
                sin(curr_lat_in_rad) * sin(other_lat_in_rad)
                + cos(curr_lat_in_rad) * cos(other_lat_in_rad) * cos(curr_long_in_rad - other_long_in_rad)
            )
        )
