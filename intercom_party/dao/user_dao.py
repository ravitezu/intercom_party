"""
User Data Access Object interface
"""
import json
import requests
from typing import Iterator, List, Tuple
from abc import ABC, abstractmethod
from intercom_party.user import User
from intercom_party.distance_utils import Coordinate, DistanceUtils


class UserDAO(ABC):
    """
    User data access object Abstract base class.
    Concrete classes can implement the get_all_users method while reading from any data source such as a text file or
    some database.
    """

    @abstractmethod
    def get_all_users(self) -> Iterator[User]:
        """
        Get all users from the data source.
        Returns: Returns an iterator consisting of User objects.
        """

    def get_all_user_with_in_range(self, from_coordinate: "Coordinate", search_radius: float) -> List[Tuple[int, str]]:
        """
        Method to return all users with the search radius.
        Args:
            from_coordinate: Point from which the distance needs to be calculated.
            search_radius: Search radius.
        Returns: Return user details as a List containing the user_id and name.
        """
        dist_utils = DistanceUtils()
        users_with_in_range = []
        for user in self.get_all_users():
            if (
                dist_utils.distance_between_two_points_on_a_sphere(from_coordinate, user.get_user_coordinates())
                < search_radius
            ):
                users_with_in_range.append((user.user_id, user.name))
        return users_with_in_range


class LocalFileReader(UserDAO):
    """
    LocalFileReader class implements UserDAO and it reads the 'User' data from a text file stored on the local disk:
    customers.txt:
        {"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}
        {"latitude": "51.92893", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}
    """

    def __init__(self, file_path: str):
        """
        Args:
            file_path: File path to
        Returns: None
        """
        self._file_path = file_path

    def get_all_users(self) -> Iterator[User]:
        """
        Actual method that reads the user data from the text file.
        Returns: Return an Iterator/Generator which reads one line/user at a time.
        """
        with open(self._file_path) as user_file:
            while True:
                line = user_file.readline()
                # If no line, break the loop.
                if not line:
                    break
                # If line, convert the json line into User object.
                user_dict = json.loads(line)
                # Initialize the User object and return using yield.
                yield User(**user_dict)


class RemoteFileReader(UserDAO):
    """
    RemoteFileReader class implements UserDAO and it reads the 'User' data file hosted somewhere on the internet.
    Expected file format:
        {"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}
        {"latitude": "51.92893", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}
    """

    def __init__(self, file_url: str):
        """
        Args:
            file_url: URL to fetch the file. Ex: https://s3.amazonaws.com/intercom-take-home-test/customers.txt
        """
        self._file_url = file_url

    def get_all_users(self) -> Iterator[User]:
        """
        Actual method that GETs the remote file.
        Returns: Return an Iterator/Generator which read one line/user at a time.
        """
        response = requests.get(self._file_url, stream=True)
        for line in response.iter_lines():
            if line:
                user_dict = json.loads(line)
                yield User(**user_dict)
