"""
This is the file that gets executed when you execute the intercom_party directory with Python.
"""
import argparse
from intercom_party.distance_utils import Coordinate
from intercom_party.dao.user_dao import LocalFileReader, RemoteFileReader

# Constants
DUBLIN_OFFICE_COORDINATES = ("53.339428", "-6.257664")
DEFAULT_WITHIN_DISTANCE_FROM_DUBLIN_OFFICE = 100.0


def get_args():
    """
    Returns: Reads and returns the command line arguments.
    """
    parser = argparse.ArgumentParser(
        description="Find users with in a search radius from our Dublin office",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--range", help="Search radius from Dublin office", default=DEFAULT_WITHIN_DISTANCE_FROM_DUBLIN_OFFICE
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--url", help="URL to read the users data")
    group.add_argument("--file", help="File path to read the users data")
    return parser.parse_args()


def main():
    """
    Entry point method that get executed when you execute the directory with Python.
    """
    reader = None
    args = get_args()

    # Check if the passed argument is a URL
    if args.url:
        reader = RemoteFileReader(file_url=args.url)

    # # if not URL, then check for filepath.
    if args.file:
        reader = LocalFileReader(file_path=args.file)

    # Dublin office coordinates
    dublin_office_coordinates = Coordinate(
        latitude=DUBLIN_OFFICE_COORDINATES[0], longitude=DUBLIN_OFFICE_COORDINATES[1]
    )
    users_with_in_range = reader.get_all_user_with_in_range(dublin_office_coordinates, float(args.range))
    users_with_in_range.sort()
    for user_detail in users_with_in_range:
        print(user_detail[0], user_detail[1])


# Execution starts here:
main()
